#!/bin/bash

MY_EXE="$1"
test="$2"

valgrind -q --error-exitcode=1 --leak-check=full --show-reachable=yes "$MY_EXE"  < "$test"

if [[ $? != 0 ]]; then
	exit 1
	else
		echo "PASSED VALUSH"
fi


exit 0

