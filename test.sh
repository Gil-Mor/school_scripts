#!/bin/bash
echo 
echo "+++++++++++++++++++++++++++++++++++++++++++++++++"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++"
echo 

check_error () {
	if [[ $? == 0 ]]; then
		return 0
	fi

	echo 
	echo "$1" " errors - ENTER to continue other key to exit"
	read ans
	echo
	if [[ $ans != "" ]]; then
		echo "bye assbag"
		echo
		make clean
		exit 1
	fi
}

NUM=$1  #like 5
LETTER=$2  # like b

EX_AND_NUM="ex"$NUM
NUM_AND_LETTER=$NUM$LETTER
EX_NUM_AND_LETTER=$EX_AND_NUM$LETTER
MY_EX_LONG="my"$EX_NUM_AND_LETTER  # name of my executable

POSTFIX="cc"


EX_PATH=~/Code/OSCourse1/$EX_AND_NUM/$EX_NUM_AND_LETTER
echo $EX_PATH

cd $EX_PATH

#------------- compilation --------------------
#make $ex                       # make - NOT USING MAKE NOW

com.sh $EX_NUM_AND_LETTER

check_error makush
#---------------------------------------------------

mv $MY_EX_LONG tests/$MY_EX_LONG          # move my exe to tests dir

TEST_PATH="$EX_PATH"/"tests"

MY_EXE="$TEST_PATH"/$MY_EX_LONG

SKUL_EXE="$TEST_PATH"/"$EX_NUM_AND_LETTER"

				# argumet 3 is test part. if entered argument then only this
				# test will be preformed.
if [[ $3 != "" ]] ; then
	PART="$3"

	test=$(find "$TEST_PATH" | grep $PART)	

	check_error test_partush

    valush.sh "$MY_EXE" "$test"

	check_error valush

	# pass the tests path to test part so we can write the output files there
	test_part.sh "$TEST_PATH" "$MY_EXE" "$SKUL_EXE" "$test"

	check_error testush

	exit 0
fi

for test in "$TEST_PATH"/ex*.in ; do
	echo "$test"

	valush.sh "$MY_EXE" "$test"

	check_error valush

	# pass the tests path to test part so we can write the output files there
	test_part.sh "$TEST_PATH" "$MY_EXE" "$SKUL_EXE" "$test"

	check_error testush
done

# make clean



