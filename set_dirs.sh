#!/bin/bash
echo
echo "================================================"
echo "================================================"
echo

			# can activate from any dir
cd ~/intro/mod2

chmod +rwx *
num="$1"
ex=ex"$1"

if [[ -d "$ex" ]]; then
	echo "this ex already exists"
	exit 1
fi

# create back up first time setting dir
if [[ ! -d backup"$ex" ]]; then
	mkdir -m=+rwx backup"$ex"

		# back up .cc files if they exist
	if [[ -f "$ex"a.cc ]]; then 
		cp "$ex"*.cc backup"$ex" 
		cp README backup"$ex"
	fi

	cp *"$ex"*.tgz backup"$ex"

	cp *"$ex"*.doc backup"$ex"

		
	#else
	#	cp * ../
fi

		# create ex dir and test dir in it
mkdir -m=+rwx "$ex"

mkdir -m=+rwx "$ex"/tests
					# unpack tar to tests dir
					# move all files from mod2 to ex dir or ex/tests
				        #!IMPORTANT! do tests.tgz first because of names problem
					# cant know the name of sol.tgz
tar -xvzf *ests*.tgz -C "$ex"/tests
mv *ests*.tgz "$ex"/tests

tar -xvzf *"$ex"*.tgz -C "$ex"/tests
mv *"$ex"*.tgz "$ex"/tests


			



mv *"$ex"*.doc "$ex"
mv README "$ex"

			# cd to ex dir and make parts dirs
cd "$ex"
mkdir -m=+rwx "$ex"{a,b,c}
#mkdir -m=+rwx "$ex"b
#mkdir -m=+rwx "$ex"c

			# mov .cc's to their dirs
mv ../"$ex"a.cc "$ex"a
mv ../"$ex"b.cc "$ex"b
mv ../"$ex"c.cc "$ex"c

			# make tests dir in each one
mkdir -m=+rwx "$ex"{a,b,c}/tests


			# move tests to part/tests dir
for file in tests/*a*; do
   cp $file "$ex"a/tests
done

for file in tests/*b*; do
   cp $file "$ex"b/tests
done

for file in tests/*c*; do
   cp $file "$ex"c/tests
done







