#!/bin/bash
echo "========================================="

TESTS_PATH="$1"

MY_EXE="$2"

SKUL_EXE="$3"

test="$4"

"$MY_EXE" < "$test" > "$TESTS_PATH"/Amystdout.txt

"$SKUL_EXE" < "$test" > "$TESTS_PATH"/Asklstdout.txt

if diff $TESTS_PATH/Amystdout.txt $TESTS_PATH/Asklstdout.txt ; then
   echo "PASSED STD"
   echo
   else
	exit 1
fi


echo "========================================="
